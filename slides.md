--- 
author: Thomas Clavier
title: La vie est belle en tests automatiques
date: <img src="./includes/cc-by-sa.svg" height="20px"> <a href="https://gitlab.com/tclavier/la-vie-est-belle-en-tests-automatiques/"><img src="./includes/fork-me-on-gitlab.svg" height="20px"></a>
---

#

| | |
|-|-|
| Thomas Clavier | ![](./includes/thomas-clavier.jpg) |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | tclavier@azae.net |
|<i class="fa fa-mastodon"></i>   | @thomas@libre-entreprise.com |
|<i class="fa fa-matrix-org"></i> | @tclavier:matrix.org |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>


# Histoire d'un petit CTO^[Chief Technical Officer] {data-background-image="./includes/cto.png" data-state="white90"}

* Éditeur d'un logiciel de génération de tests depuis des modèles.
* 30 salariés (12 développeurs, 1 product owner, 3 administratifs, 14 commerce / support)

![](./includes/gie-carte-bancaire.jpg){height=10%}
![](./includes/qualitycenter.png){height=10%}
![](./includes/ratp.png){height=10%}

# Zero bug chez le client {data-background-image="./includes/icecream.jpg" data-state="white70"}

* 1 dev qui maintient à temps plein des tests de bout en bout.
* 1 campagne de tests exploratoires avec l'ensemble de l'entreprise (3 semaines tous les 3 à 6 mois).
* 3000 bug dans JIRA

# Inverser la pyramide {data-background-image="./includes/icecream-drop.jpg" data-state="white60"}
 
# Stratégie {data-background-image="./includes/strategie.jpg" data-state="white80"}

* Changer sans perdre la qualité
* Changer l'état d'esprit

# Changer sans perdre la qualité {data-background-image="./includes/andon.jpg" data-state="white80"}

* Ne pas toucher au processus actuel
* Ajouter un Andon^[行灯 en japonais, pourrait se traduire par "lumière où il faut aller". Signal d'alarme permetant d'arrêter la production].

# {data-background-image="./includes/speedometer.jpg" data-state="white80"}

> « Lorsqu’une mesure devient un objectif, elle cesse d’être une bonne mesure »

Loi de Goodhart

# Changer l'état d'esprit {data-background-image="./includes/worker.jpg" data-state="white80"}

> Tout le monde est responsable de la qualité, 

* 15 minutes de Toyota Kata^[Exercice d'entrainement à l'apprentissage] par jour 
* 1 objectif commun = le résultat de la société 
* Suppression des primes et des indicateurs d'équipe, remplacés par la participation aux bénéfices

# Analyse des bugs {data-background-image="./includes/crisis.jpg" data-state="white80"}

Objectif : éradiquer des familles de bug.

* Programmation immuable
* Suppression des "god classes" ou équivalent
* Généralisation du TDD^[Test Driven Development] et de la programmation en binôme (pair programming)
* Élaboration d'un DSL^[Domain Specific Langage, langage de description des tests adapté à notre métier] de test

# Quelques mois plus tard {data-background-image="./includes/vieux.jpg" data-state="white80"}

> « Ça fait 4 jours que je fais du test exploratoire et je n'ai trouvé aucun bug. J'ai vraiment l'impression de perdre mon temps. »

# En chiffres {data-background-image="./includes/metrologie.jpg" data-state="white80"}

* 2 ans et demis de travail.
* 10 bug par an.
* Est considéré comme bug toute anomalie détectée après l'annonce du "fini" par le dev


# Merci {data-background-image="./includes/love-customers.png" data-state="white80"}

> Cultivons l'amour de nos clients.

<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | tclavier@azae.net |
|<i class="fa fa-mastodon"></i>   | @thomas@libre-entreprise.com |
|<i class="fa fa-matrix-org"></i> | @tclavier:matrix.org |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>
